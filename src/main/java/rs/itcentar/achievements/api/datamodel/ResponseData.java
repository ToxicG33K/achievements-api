package rs.itcentar.achievements.api.datamodel;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author milos
 */
public class ResponseData<E> {
    private E data;
    private List<String> error = new ArrayList<>();

    public ResponseData() {
    }

    public ResponseData(E data) {
        this.data = data;
    }

    public E getData() {
        return data;
    }

    public void setData(E data) {
        this.data = data;
    }

    public List<String> getError() {
        return error;
    }

    public void addError(String error) {
        this.error.add(error);
    }
}
