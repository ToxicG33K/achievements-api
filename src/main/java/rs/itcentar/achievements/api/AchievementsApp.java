package rs.itcentar.achievements.api;

import java.util.logging.Logger;
import org.glassfish.jersey.server.ResourceConfig;
import rs.itcentar.achievements.api.resources.AchievementsResource;

public class AchievementsApp extends ResourceConfig {
    private Logger logger = Logger.getGlobal();
    
    public AchievementsApp() {
        packages("rs.itcentar.achievements.api.resources");
        register(AchievementsResource.class);
    }
}
