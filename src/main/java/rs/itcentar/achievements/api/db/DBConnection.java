package rs.itcentar.achievements.api.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConnection {

    private static final DBConfig CONFIG = new DBConfig();

    public static final Connection getConnection() {
        try {
            Class.forName(CONFIG.getJDBCDriver()).newInstance();
            return DriverManager.getConnection(CONFIG.getJDBCUrl(),
                    CONFIG.getJDBCUser(),
                    CONFIG.getJDBCPassword());
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException | SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}
