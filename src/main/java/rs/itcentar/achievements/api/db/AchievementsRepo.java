package rs.itcentar.achievements.api.db;

import rs.itcentar.achievements.api.datamodel.Achievement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Comparator;
import java.util.UUID;
import rs.itcentar.achievements.api.datamodel.Game;

public class AchievementsRepo {

    public static int createAchievement(String gameId, Achievement achiv) throws SQLException {
        int ret = -1;
        String id = UUID.randomUUID().toString();
        try (Connection conn = DBConnection.getConnection()) {
            int nextDisplayOrder = -1;
            try (PreparedStatement ps = conn.prepareStatement("SELECT MAX(displayOrder) AS lastDisplayOrder FROM achievements\n" +
                "INNER JOIN games_has_achievements ON games_has_achievements.achievements_id=achievements.id\n" +
                "INNER JOIN games ON games_has_achievements.games_id=games.id\n" +
                "WHERE games.id=?\n" +
                "ORDER BY displayOrder ASC\n" +
                "LIMIT 1")) {
                ps.setString(1, gameId);
                ResultSet rs = ps.executeQuery();
                rs.next();
                nextDisplayOrder = rs.getInt("lastDisplayOrder");
            }
            if(nextDisplayOrder >= 0) {
                try (PreparedStatement ps = conn.prepareStatement("INSERT INTO achievements (id, displayName, description, icon, displayOrder, created, updated) VALUES (?, ?, ?, ?, ?, ?, ?)")) {
                    ps.setString(1, id);
                    ps.setString(2, achiv.getDisplayName());
                    ps.setString(3, achiv.getDescription());
                    ps.setString(4, achiv.getIcon());
                    ps.setInt(5, nextDisplayOrder+1);
                    ps.setLong(6, System.currentTimeMillis());
                    ps.setLong(7, 0);
                    ret = ps.executeUpdate();
                }
                if (ret == 1) {
                    try (PreparedStatement ps = conn.prepareStatement("INSERT INTO games_has_achievements(games_id, achievements_id) VALUES (?, ?)")) {
                        ps.setString(1, gameId);
                        ps.setString(2, id);
                        ret = ps.executeUpdate();
                    }
                }
            }
        }
        return ret;
    }

    public static List<Achievement> getAllGameAchievements(String gameId) throws SQLException {
        try (Connection conn = DBConnection.getConnection()) {
            PreparedStatement statement = conn.prepareStatement(
                    "SELECT achievements.*\n"
                    + "FROM achievements\n"
                    + "INNER JOIN games_has_achievements ON games_has_achievements.achievements_id=achievements.id\n"
                    + "INNER JOIN games ON games_has_achievements.games_id=games.id\n"
                    + "WHERE games.id=?\n"
                    + "ORDER BY displayOrder ASC");
            statement.setString(1, gameId);
            ResultSet rs = statement.executeQuery();
            List<Achievement> achiv = new ArrayList<>();
            while (rs.next()) {
                String id = rs.getString("id");
                String displayName = rs.getString("displayName");
                String description = rs.getString("description");
                String icon = rs.getString("icon");
                int displayOrder = rs.getInt("displayOrder");
                long created = rs.getLong("created");
                long updated = rs.getLong("updated");
                Achievement a = new Achievement(id, displayName, description, icon, displayOrder, created, updated);
                achiv.add(a);
            }
            Collections.sort(achiv, new Comparator<Achievement>() {
                @Override
                public int compare(Achievement o1, Achievement o2) {
                    return Integer.compare(o1.getDisplayOrder(), o2.getDisplayOrder());
                }
            });
            return achiv;
        }
    }

    public static Achievement getAchievement(String id) throws SQLException {
        try (Connection conn = DBConnection.getConnection()) {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM achievements WHERE id = ?");
            statement.setString(1, id);
            ResultSet rs = statement.executeQuery();
            rs.next();
            String displayName = rs.getString("displayName");
            String description = rs.getString("description");
            String icon = rs.getString("icon");
            int displayOrder = rs.getInt("displayOrder");
            long created = rs.getLong("created");
            long updated = rs.getLong("updated");
            Achievement a = new Achievement(id, displayName, description, icon, displayOrder, created, updated);
            return a;
        }
    }

    public static int updateAchievement(String id, Achievement achiv) throws SQLException {
        try (Connection conn = DBConnection.getConnection()) {
            int ret;
            try (PreparedStatement ps = conn.prepareStatement("UPDATE achievements SET displayName = ?, description = ?, Icon = ?, displayOrder = ?, created = ?, updated = ? WHERE id = ?")) {
                ps.setString(1, achiv.getDisplayName());
                ps.setString(2, achiv.getDescription());
                ps.setString(3, achiv.getIcon());
                ps.setInt(4, achiv.getDisplayOrder());
                ps.setLong(5, achiv.getCreated());
                ps.setLong(6, achiv.getUpdated());
                ps.setString(7, id);
                ret = ps.executeUpdate();
            }
            return ret;
        }
    }

    public static int deleteAchievement(String id) throws SQLException {
        try (Connection conn = DBConnection.getConnection()) {
            int ret;
            try (PreparedStatement ps = conn.prepareStatement("DELETE FROM achievements WHERE id = ?")) {
                ps.setString(1, id);
                ret = ps.executeUpdate();
            }
            return ret;
        }
    }

    public static boolean gameExists(String gameId) throws SQLException {
        try (Connection conn = DBConnection.getConnection()) {
            PreparedStatement statement = conn.prepareStatement("SELECT id FROM games WHERE id = ?");
            statement.setString(1, gameId);
            ResultSet rs = statement.executeQuery();
            return rs.next();
        }
    }

    public static boolean achievementExists(String achievementId) throws SQLException {
        try (Connection conn = DBConnection.getConnection()) {
            PreparedStatement statement = conn.prepareStatement("SELECT id FROM achievements WHERE id = ?");
            statement.setString(1, achievementId);
            ResultSet rs = statement.executeQuery();
            return rs.next();
        }
    }
    
    public static List<Game> getGames() throws SQLException {
        List<Game> games = new ArrayList<>();
        try (Connection conn = DBConnection.getConnection()) {
            PreparedStatement statement = conn.prepareStatement("SELECT * FROM games");
            ResultSet rs = statement.executeQuery();
            while(rs.next()) {
                String id = rs.getString("id");
                String displayName = rs.getString("gameDisplayName");
                Game g = new Game(id, displayName);
                games.add(g);
            }
            return games;
        }
    }
    
    public static int createGame(Game game) throws SQLException {
        int ret = -1;
        String id = UUID.randomUUID().toString();
        try (Connection conn = DBConnection.getConnection()) {
            try (PreparedStatement ps = conn.prepareStatement("INSERT INTO games (id, gameDisplayName) VALUES (?, ?)")) {
                ps.setString(1, id);
                ps.setString(2, game.getDisplayName());
                ret = ps.executeUpdate();
            }
        }
        return ret;
    }
    
    public static int deleteGame(String gameId) throws SQLException {
        try (Connection conn = DBConnection.getConnection()) {
            int ret;
            try (PreparedStatement ps = conn.prepareStatement("DELETE FROM games WHERE id = ?")) {
                ps.setString(1, gameId);
                ret = ps.executeUpdate();
            }
            return ret;
        }
    }
}
