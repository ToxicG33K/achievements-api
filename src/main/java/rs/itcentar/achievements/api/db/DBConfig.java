package rs.itcentar.achievements.api.db;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBConfig {
    private Properties prop = new Properties();
    private InputStream input = null;
    
    public DBConfig() {
        try {
            input = DBConfig.class.getClassLoader().getResourceAsStream("db_config.properties");
            prop.load(input);
        } catch (IOException ex) {
            Logger.getLogger(DBConfig.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public String getJDBCDriver(){
        return prop.getProperty("jdbc.driver");
    }
    
    public String getJDBCUrl(){
        return prop.getProperty("jdbc.url");
    }
    
    public String getJDBCUser(){
        return prop.getProperty("jdbc.user");
    }
    
    public String getJDBCPassword(){
        return prop.getProperty("jdbc.password");
    }
}