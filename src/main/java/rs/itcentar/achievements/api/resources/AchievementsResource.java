package rs.itcentar.achievements.api.resources;

import com.fasterxml.jackson.databind.SerializationFeature;
import rs.itcentar.achievements.api.datamodel.Achievement;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import rs.itcentar.achievements.api.db.AchievementsRepo;
import java.sql.SQLException;
import org.glassfish.jersey.jackson.internal.jackson.jaxrs.annotation.JacksonFeatures;
import rs.itcentar.achievements.api.datamodel.Game;
import rs.itcentar.achievements.api.datamodel.ResponseData;

@Path("achievements")
public class AchievementsResource {
    // http://localhost:8080/achievements-api/api/achievements/*

    /**
     * Get all achievements for game
     * 
     * @param gameId
     * @return 
     */
    @Path("/all/{gameId}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @JacksonFeatures(serializationEnable = SerializationFeature.INDENT_OUTPUT,
            serializationDisable = SerializationFeature.FAIL_ON_EMPTY_BEANS)
    public Response getAllGameAchievements(@NotNull @PathParam("gameId") String gameId) {
        ResponseData<List<Achievement>> response = new ResponseData<>();
        try {
            if (!AchievementsRepo.gameExists(gameId)) {
                response.addError(String.format("Game %s doesn't exist", gameId));
            }
            List<Achievement> achievements = AchievementsRepo.getAllGameAchievements(gameId);
            response.setData(achievements);
        } catch (SQLException ex) {
            response.addError(ex.getMessage());
        }
        return Response.ok(response).build();
    }

    /**
     * Get achievement by id
     * 
     * @param id
     * @return 
     */
    @Path("/{id}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @JacksonFeatures(serializationEnable = SerializationFeature.INDENT_OUTPUT,
            serializationDisable = SerializationFeature.FAIL_ON_EMPTY_BEANS)
    public Response getAchievement(@NotNull @PathParam("id") String id) {
        ResponseData<Achievement> response = new ResponseData<>();
        try {
            if(!AchievementsRepo.achievementExists(id)) {
                response.addError(String.format("Achievement %s doesn't exist", id));
            }
            Achievement a = AchievementsRepo.getAchievement(id);
            response.setData(a);
        } catch (SQLException ex) {
            response.addError(ex.getMessage());
        }
        return Response.ok(response).build();
    }

    /**
     * Create achievement
     * 
     * @param gameId
     * @param achiv
     * @return 
     */
    @Path("/{gameId}")
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    public Response createAchievement(@NotNull @PathParam("gameId") String gameId, @NotNull Achievement achiv) {
        ResponseData<Integer> response = new ResponseData<>();
        try {
            if(AchievementsRepo.achievementExists(achiv.getId())) {
                response.addError(String.format("Achievement %s exists", achiv.getDisplayName()));
            }
            int ret = AchievementsRepo.createAchievement(gameId, achiv);
            response.setData(ret);
        } catch (SQLException ex) {
            response.addError(ex.getMessage());
        }
        return Response.ok(response).build();
    }

    /**
     * Delete achievement by id
     * 
     * @param id
     * @return 
     */
    @Path("/{id}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAchievement(@NotNull @PathParam("id") String id) {
        ResponseData<Integer> response = new ResponseData<>();
        try {
            if(!AchievementsRepo.achievementExists(id)) {
                response.addError(String.format("Achievement %s doesn't exist", id));
            }
            int ret = AchievementsRepo.deleteAchievement(id);
            response.setData(ret);
        } catch (SQLException ex) {
            response.addError(ex.getMessage());
        }
        return Response.ok(response).build();
    }

    /**
     * Update achievement
     * 
     * @param id
     * @param achiv
     * @return 
     */
    @Path("/{id}")
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateAchievement(@NotNull @PathParam("id") String id, Achievement achiv) {
        ResponseData<Integer> response = new ResponseData<>();
        try {
            if(!AchievementsRepo.achievementExists(id)) {
                response.addError(String.format("Achievement %s doesn't exist", id));
            }
            achiv.setId(id);
            int ret = AchievementsRepo.updateAchievement(id, achiv);
            response.setData(ret);
            
        } catch (SQLException ex) {
            response.addError(ex.getMessage());
        }
        return Response.ok(response).build();
    }
    
    /**
     * Get all games
     * 
     * @return games
     */
    @Path("/games")
    @GET
    @Produces({MediaType.APPLICATION_JSON})
    public Response getGames() {
        ResponseData<List<Game>> response = new ResponseData<>();
        try {
            List<Game> games = AchievementsRepo.getGames();
            response.setData(games);
        } catch (SQLException ex) {
            response.addError(ex.getMessage());
        }
        return Response.ok(response).build();
    }
    
    /**
     * Create game
     * 
     * @param game game object
     * @return 
     */
    @Path("games")
    @PUT
    @Produces({MediaType.APPLICATION_JSON})
    public Response createGame(@NotNull Game game) {
        ResponseData<Integer> response = new ResponseData<>();
        try {
            if(AchievementsRepo.gameExists(game.getId())) {
                response.addError(String.format("Game %s exists", game.getDisplayName()));
            }
            int ret = AchievementsRepo.createGame(game);
            response.setData(ret);
        } catch (SQLException ex) {
            response.addError(ex.getMessage());
        }
        return Response.ok(response).build();
    }
    
    /**
     * Delete game by id
     * 
     * @param gameId
     * @return 
     */
    @Path("games/{gameId}")
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteGame(@NotNull @PathParam("gameId") String gameId) {
        ResponseData<Integer> response = new ResponseData<>();
        try {
            if(!AchievementsRepo.gameExists(gameId)) {
                response.addError(String.format("Game %s doesn't exist", gameId));
            }
            int ret = AchievementsRepo.deleteGame(gameId);
            response.setData(ret);
        } catch (SQLException ex) {
            response.addError(ex.getMessage());
        }
        return Response.ok(response).build();
    }
}
