-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 22, 2018 at 02:05 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `achievements_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `achievements`
--

CREATE TABLE `achievements` (
  `id` varchar(255) NOT NULL,
  `displayName` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `icon` varchar(255) NOT NULL,
  `displayOrder` int(10) NOT NULL,
  `created` bigint(20) NOT NULL,
  `updated` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `achievements`
--

INSERT INTO `achievements` (`id`, `displayName`, `description`, `icon`, `displayOrder`, `created`, `updated`) VALUES
('0bca90dd-3e32-42c7-9c8f-df33bf4e2db1', 'Get the Party Started', 'Kingdom Rush Origins - Win stage 1 with 3 stars', '', 6, 20180505023727, 20180506043746),
('14fe7423-6e81-4d23-9568-0ddf639e7aa3', 'Mean Time', 'Overclocked: The Aclockalypse Achievements - Defeat Greenwich\r\n', '', 5, 20180505023512, 20180506043741),
('1ea0317c-dd61-43c0-931c-a568dd87a4e4', 'On the Clock', 'Overclocked: The Aclockalypse Achievements - Survive the Clockface Battle\r\n', '', 4, 20180505023512, 20180506043735),
('30fc8a58-2bf0-4fd4-897e-f7a7f594564e', 'Gold Farmer', 'Crusaders of Light - Achievement can be acquired at any time and is not tied to any specific event -Receive 1t total gold', '', 3, 20180505023512, 20180506043730),
('35455839-81bd-4e4b-8bac-5b85938d24a7', 'Taking Inventory', 'MineCraft - Open your inventory. The description will match the configured inventory key.', '', 13, 20180505130257, 20180506043853),
('4d4ae7d0-f1f2-48e5-998c-03ecd28752f4', 'Getting Wood', 'Pick up wood from the ground.', '', 12, 20180505130257, 20180506043850),
('677c8ee0-3a40-4be4-bb37-1777f82a40ff', 'Champion of Wesnoth', 'Battle For Wesnoth - Complete a full campaign on hard difficulty', '', 7, 20180505023741, 20180506043753),
('6aba4c57-27b3-4a6e-9b95-433007e6912d', 'Pick up 100 runes', 'Dota - Achievement is earned by performing task in the world of Dota', '', 2, 20180505025613, 20180505113503),
('75f42be2-c95e-4a9c-8edd-3cf40a2b41d9', 'Battle Master', 'Battle For Wesnoth - Kill 100 enemy units', '', 9, 20180505025350, 20180506043820),
('7bde621f-5fdf-4cfb-9109-2f1f9897b504', 'Fearless Leader ', 'Battle For Wesnoth - Level up your leader', '', 8, 20180505024948, 20180506043815),
('8bc15810-003c-45c9-9d96-61e64ecf600b', 'Consume 300 trees using Tangos', 'Dota - Achievement is earned by performing task in the world of Dota', 'https://d1u5p3l4wpay3k.cloudfront.net/dota2_gamepedia/9/90/Threehundred.png?version=b8565357b1d42eb195c79bd64ff25dfd', 1, 20180505023512, 20180505113523),
('e0897484-0f44-441b-9a7a-7c9611b0343f', 'Chop tree', 'Terraria Achievement - Chop down your first tree', '', 11, 20180505023812, 20180506043843),
('f6cf1153-8a45-450c-bb54-ef77d41c42ef', 'Captain''s Phaser Master', 'Star Trek - Killed 100 enemies with the Captain''s Phaser', '', 10, 20180505023819, 20180506043838),
('fc9024a7-bf17-4b4c-b884-ee7a542faa50', 'Champion of Wesnoth', 'Battle For Wesnoth - Complete a full campaign on hard difficulty', '', 14, 1532260978047, 0);

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `id` varchar(255) NOT NULL,
  `gameDisplayName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `gameDisplayName`) VALUES
('09af38f8-28d1-4210-85ae-9bdbdbb09f13', 'Minecraft'),
('1d94a998-39c0-47d2-be11-13f5dc0e4aff', 'Terraria'),
('5abd8367-5a51-4fd6-957a-37fdb06e7644', 'Dota'),
('5c4a9a58-9855-4957-b77e-169438485feb', 'Star Trek'),
('9a2b5556-465b-4ff8-8fb2-c773ed247e30', 'Kingdom Rush Origins'),
('b036b190-2265-4343-9a92-56c2bf153ff8', 'Crusaders of Light'),
('c4694c33-cc43-47d6-b205-2a119a8fc532', 'Overclocked: The Aclockalypse'),
('e2334d45-48e5-49a4-b502-d91fbc66ab2f', 'Battle for Wesnoth');

-- --------------------------------------------------------

--
-- Table structure for table `games_has_achievements`
--

CREATE TABLE `games_has_achievements` (
  `games_id` varchar(255) NOT NULL,
  `achievements_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `games_has_achievements`
--

INSERT INTO `games_has_achievements` (`games_id`, `achievements_id`) VALUES
('1d94a998-39c0-47d2-be11-13f5dc0e4aff', '8bc15810-003c-45c9-9d96-61e64ecf600b'),
('1d94a998-39c0-47d2-be11-13f5dc0e4aff', 'e0897484-0f44-441b-9a7a-7c9611b0343f'),
('c4694c33-cc43-47d6-b205-2a119a8fc532', '7bde621f-5fdf-4cfb-9109-2f1f9897b504'),
('e2334d45-48e5-49a4-b502-d91fbc66ab2f', '4d4ae7d0-f1f2-48e5-998c-03ecd28752f4'),
('e2334d45-48e5-49a4-b502-d91fbc66ab2f', 'fc9024a7-bf17-4b4c-b884-ee7a542faa50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `achievements`
--
ALTER TABLE `achievements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `games_has_achievements`
--
ALTER TABLE `games_has_achievements`
  ADD PRIMARY KEY (`games_id`,`achievements_id`),
  ADD KEY `fk_games_has_achievements_achievements1_idx` (`achievements_id`),
  ADD KEY `fk_games_has_achievements_games_idx` (`games_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `games_has_achievements`
--
ALTER TABLE `games_has_achievements`
  ADD CONSTRAINT `fk_games_has_achievements_achievements1` FOREIGN KEY (`achievements_id`) REFERENCES `achievements` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_games_has_achievements_games` FOREIGN KEY (`games_id`) REFERENCES `games` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
